import h5py
import numpy as np

train = [
    "D:\Baza danych\dataset_part1.h5",
    "D:\Baza danych\dataset_part2.h5",
    "D:\Baza danych\dataset_part3.h5",
]
val = ["D:\Baza danych\dataset_part4.h5"]
test = ["D:\Baza danych\dataset_part5.h5"]

data_folder = "D:\Baza danych\part1_dataset"


class Chunk:
    def __init__(self, chunk_size=100, save=False):
        self.chunk_size = chunk_size
        self.save = save

    def chunk_all(self):
        self.chunk_train()
        self.chunk_val()
        self.chunk_test()

    def chunk_train(self):
        counter = 1
        for dat in train:
            _train_x = None
            _train_y = None
            hf = h5py.File(dat, "r")
            x = hf.get("image_data")
            y = hf.get("labels")
            _train_x = x.value
            _train_y = y.value
            hf.close()
            _train_x_res = []
            for img in _train_x:
                _train_x_res.append(
                    np.stack((np.resize(img, [128, 128]),) * 3, axis=-1)
                )
            _train_x = []
            _train_x_res = np.array(_train_x_res)
            _train_y = [float(item) for item in _train_y]
            leng = _train_x_res.shape[0]
            chunks = leng // self.chunk_size
            if self.save:
                print(f"Saving training {chunks} chunks")
                for i in range(chunks):
                    x = _train_x_res[
                        i * self.chunk_size : (i + 1) * self.chunk_size, :, :, :
                    ]
                    y = _train_y[i * self.chunk_size : (i + 1) * self.chunk_size]
                    with open(f"{data_folder}/train/id-tr-{counter}.npy", "wb") as f:
                        np.save(f, x)
                        np.save(f, y)
                    print(
                        f"Chunk {counter} saved (values {i*self.chunk_size} to {(i+1)*self.chunk_size}!"
                    )
                    counter += 1
                x = _train_x_res[chunks * self.chunk_size :, :, :, :]
                y = _train_y[chunks * self.chunk_size :]
                with open(f"{data_folder}/train/id-tr-{counter}.npy", "wb") as f:
                    np.save(f, x)
                    np.save(f, y)
                print(f"Chunk {counter} saved!")
                counter += 1
        _train_x = None
        _train_y = None
        _train_x_res = None

    def chunk_val(self):
        counter = 1
        _val_x = []
        for dat in val:
            hf = h5py.File(dat, "r")
            x = hf.get("image_data")
            y = hf.get("labels")
            _val_x = x.value
            _val_y = y.value
            hf.close()
            _val_x_res = []
            for img in _val_x:
                _val_x_res.append(np.stack((np.resize(img, [128, 128]),) * 3, axis=-1))
            _val_x = []
            _val_x_res = np.array(_val_x_res)
            _val_y = [float(item) for item in _val_y]
            leng = _val_x_res.shape[0]
            chunks = leng // self.chunk_size
            if self.save:
                print(f"Saving validation {chunks} chunks")
                for i in range(chunks):
                    x = _val_x_res[
                        i * self.chunk_size : (i + 1) * self.chunk_size, :, :, :
                    ]
                    y = _val_y[i * self.chunk_size : (i + 1) * self.chunk_size]
                    with open(f"{data_folder}/valid/id-val-{counter}.npy", "wb") as f:
                        np.save(f, x)
                        np.save(f, y)
                    print(
                        f"Chunk {counter} saved (values {i*self.chunk_size} to {(i+1)*self.chunk_size}!"
                    )
                    counter += 1
                x = _val_x_res[chunks * self.chunk_size :, :, :, :]
                y = _val_y[chunks * self.chunk_size :]
                with open(f"{data_folder}/valid/id-val-{counter}.npy", "wb") as f:
                    np.save(f, x)
                    np.save(f, y)
                print(f"Chunk {counter} saved!")
                counter += 1
        _val_x = None
        _val_y = None
        _val_x_res = None

    def chunk_test(self):
        counter = 1
        _test_x = []
        for dat in test:
            hf = h5py.File(dat, "r")
            x = hf.get("image_data")
            y = hf.get("labels")
            _test_x = x.value
            _test_y = y.value
            hf.close()
            _test_x_res = []
            for img in _test_x:
                _test_x_res.append(np.stack((np.resize(img, [128, 128]),) * 3, axis=-1))
            _test_x = []
            _test_x_res = np.array(_test_x_res)
            _test_y = [float(item) for item in _test_y]
            leng = _test_x_res.shape[0]
            chunks = leng // self.chunk_size
            if self.save:
                print(f"Saving test {chunks} chunks")
                for i in range(chunks):
                    x = _test_x_res[
                        i * self.chunk_size : (i + 1) * self.chunk_size, :, :, :
                    ]
                    y = _test_y[i * self.chunk_size : (i + 1) * self.chunk_size]
                    with open(f"{data_folder}/test/id-te-{counter}.npy", "wb") as f:
                        np.save(f, x)
                        np.save(f, y)
                    print(
                        f"Chunk {counter} saved (values {i*self.chunk_size} to {(i+1)*self.chunk_size}!"
                    )
                    counter += 1
                x = _test_x_res[chunks * self.chunk_size :, :, :, :]
                y = _test_y[chunks * self.chunk_size :]
                with open(f"{data_folder}/test/id-te-{counter}.npy", "wb") as f:
                    np.save(f, x)
                    np.save(f, y)
                print(f"Chunk {counter} saved!")
                counter += 1
        _test_x = None
        _test_y = None
        _test_x_res = None


if __name__ == "__main__":
    obj = Chunk(save=True)
    obj.chunk_all()
