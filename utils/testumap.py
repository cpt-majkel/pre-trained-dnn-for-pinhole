# from sklearn.model_selection import train_test_split
import math
import os

import h5py
import matplotlib.pyplot as plt
import numpy as np
import umap

# import requests

# libraries for clustering
# import hdbscan
# import sklearn.cluster as cluster
# from sklearn.metrics import adjusted_rand_score, adjusted_mutual_info_score

data = "D:\Baza danych\dataset_part1.h5"

hf = h5py.File(data, "r")
x = hf.get("image_data")
y = hf.get("labels")
x = x[()]
y = y[()]
hf.close()
_train_x_res = []
for img in x:
    _train_x_res.append(np.stack((np.resize(img, [128, 128]),) * 3, axis=-1))

images = np.array(_train_x_res)
labels = y.astype(dtype=np.float64)

X_train = np.empty([math.floor(len(labels) / 100), 49152], dtype=np.float64)
y_train = np.empty([math.floor(len(labels) / 100)], dtype=np.float64)
X_test = np.empty([math.floor(len(labels) / 100), 49152], dtype=np.float64)
y_test = np.empty([math.floor(len(labels) / 100)], dtype=np.float64)
# Get a subset of the data
for i in range(math.floor(len(labels) / 100)):
    X_train[i, :] = np.array(np.ndarray.flatten(images[i, :, :, :]), dtype=np.float64)
    y_train[i] = labels[i]
    X_test[i, :] = np.array(
        np.ndarray.flatten(images[i + math.floor(len(labels) / 100), :, :, :]),
        dtype=np.float64,
    )
    y_test[i] = labels[i + math.floor(len(labels) / 100)]

# Plot distribution
classes, frequency = np.unique(y_train, return_counts=True)
fig = plt.figure(1, figsize=(4, 4))
plt.clf()
plt.bar(classes, frequency)
plt.xlabel("Class")
plt.ylabel("Frequency")
plt.title("Data Subset")
plt.show()
# 2D Embedding
## UMAP
reducer = umap.UMAP(
    n_components=2, n_neighbors=15, random_state=42, transform_seed=42, verbose=False
)
reducer.fit(X_train)

galaxy10_umap = reducer.transform(X_train)
fig1 = plt.figure(1, figsize=(4, 4))
plt.clf()
plt.scatter(
    galaxy10_umap[:, 0],
    galaxy10_umap[:, 1],
    c=y_train,
    label=y_train,
)
plt.title("Standard UMAP ")
# plt.colorbar(boundaries=np.arange(11) - 0.5).set_ticks(np.arange(10))
plt.show()

### UMAP - Supervised
reducer = umap.UMAP(
    n_components=2, n_neighbors=15, random_state=42, transform_seed=42, verbose=False
)
reducer.fit(X_train, y_train)

galaxy10_umap_supervised = reducer.transform(X_train)
fig2 = plt.figure(1, figsize=(4, 4))
plt.clf()
plt.scatter(
    galaxy10_umap_supervised[:, 0],
    galaxy10_umap_supervised[:, 1],
    c=y_train,
    label=y_train,
)
plt.title("Supervised UMAP")
# plt.colorbar(boundaries=np.arange(11) - 0.5).set_ticks(np.arange(10))
plt.show()
