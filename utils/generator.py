import keras
import numpy as np
from tensorflow.keras.applications.efficientnet import preprocess_input
from tensorflow.keras.utils import Sequence

data_folder = "D:\Baza danych\part1_dataset/"


class DataGenerator(Sequence):
    def __init__(
        self,
        list_IDs,
        sub,
        batch_size=100,
        dim=(100, 128, 128),
        file_batch=100,
        n_channels=3,
        n_classes=2,
        to_fit=True,
    ):
        self.dim = dim
        self.subfoler = sub
        self.batch_size = batch_size
        self.file_batch = file_batch
        self.list_IDs = list_IDs
        self.n_classes = n_classes
        self.n_channels = n_channels
        self.to_fit = to_fit
        self.indexes = np.arange(len(self.list_IDs))

    def __len__(self):
        """Denotes the number of batches per epoch
        :return: number of batches per epoch
        """
        return int((np.floor(len(self.list_IDs) * self.file_batch) / self.batch_size))

    def __getitem__(self, index):
        """Generate one batch of data
        :param index: index of the batch
        :return: X and y when fitting. X only when predicting
        """
        # Generate indexes of the batch
        num_of_files = self.batch_size // self.file_batch
        indexes = self.indexes[index * num_of_files : (index + 1) * num_of_files]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        if self.to_fit:
            X, y = self.__data_generation(list_IDs_temp)
            return X, y
        else:
            X = self.__data_generation(list_IDs_temp)
            return X

    def __data_generation(self, list_IDs_temp):
        "Generates data containing batch_size samples"  # X : (n_samples, *dim, n_channels)
        # TODO: allow larger batch
        X = []
        y = []
        for i, ID in enumerate(list_IDs_temp):
            with open(data_folder + self.subfoler + "/" + ID + ".npy", "rb") as f:
                _x = np.load(f)
                _y = np.load(f)
            for el in _x:
                preprocess_input(el)
            # Store sample
            X = _x
            # Store class
            y = _y

        if self.to_fit:
            return X, keras.utils.to_categorical(y, num_classes=self.n_classes)
        else:
            return X
