import os
import random

import numpy as np
import tensorflow as tf
from keras.layers import Dense, Dropout, Flatten
from keras.models import Model, Sequential
from tensorflow.keras.applications import EfficientNetB0

from utils.generator import DataGenerator

os.environ["TF_DETERMINISTIC_OPS"] = "1"

random.seed(42)
np.random.seed(42)
tf.random.set_seed(42)

data_folder = "D:\Baza danych\part1_dataset/"


def main():

    partition = {"train": [], "validation": [], "test": []}

    for file in os.listdir(data_folder + "train"):
        if file.startswith("id-tr"):
            partition["train"].append(str(file.split(".")[0]))
    for file in os.listdir(data_folder + "valid"):
        if file.startswith("id-val"):
            partition["validation"].append(str(file.split(".")[0]))
    for file in os.listdir(data_folder + "test"):
        if file.startswith("id-te"):
            partition["test"].append(str(file.split(".")[0]))

    train_generator = DataGenerator(partition["train"], sub="train")
    valid_generator = DataGenerator(partition["validation"], sub="valid")
    test_generator = DataGenerator(partition["test"], sub="test")

    base_model = EfficientNetB0(
        weights="imagenet", include_top=False, input_shape=(128, 128, 3), classes=2
    )

    top_model = Sequential()
    top_model.add(Dense(500, activation="relu"))
    top_model.add(Dropout(0.5))
    top_model.add(Dense(2, activation="softmax"))

    x = base_model.output
    x = Flatten()(x)
    predictions = top_model(x)

    model = Model(inputs=base_model.input, outputs=predictions)

    for layer in base_model.layers:
        layer.trainable = False

    model.compile(loss="binary_crossentropy", optimizer="adam", metrics=["accuracy"])

    model.summary()

    model.fit(x=train_generator, validation_data=valid_generator, epochs=10)

    model.evaluate(test_generator, verbose=1)


if __name__ == "__main__":
    main()
